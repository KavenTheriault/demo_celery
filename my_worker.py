from celery import Celery

app = Celery('my_tasks', broker='sqla+sqlite:////home/admin/demo_celery/db.sqlite')

@app.task
def add(x, y):
    return x + y