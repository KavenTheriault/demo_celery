### Configuration (Debian)
For this test, the project directory is `/home/admin/demo_celery/`
```
sudo apt-get update && sudo apt-get upgrade -y
sudo apt-get install -y \
  python3 \
  python3-pip
sudo pip3 install virtualenv

git clone {repo}
cd demo_celery

source venv/bin/activate
pip install -r requirements.txt

sudo mkdir /etc/conf.d
sudo cp conf.d/celery /etc/conf.d/celery
sudo cp systemd/celery.service /etc/systemd/system/celery.service

sudo useradd celery
sudo groupadd celery
sudo usermod -a -G celery celery

sudo mkdir /var/log/celery
sudo chown -R celery:celery /var/log/celery

sudo mkdir /var/run/celery
sudo chown -R celery:celery /var/run/celery

sudo chown -R celery:celery /home/admin/demo_celery/

sudo systemctl daemon-reload
sudo systemctl start celery.service
```
### Notes
The user who is queueing the request must have access to the broker
- If `sqlite`, the user must have write access to .sqlite file

##### Manual start of the worker
```
celery -A my_worker worker --loglevel=info
```